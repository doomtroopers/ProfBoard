﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RabbitWriterService
{
    public class ControllerOption
    {
        public string Name { get; set; }

        public int PeriodAutoResend { get; set; }

        public double MinRangeLimit { get; set; }

        public double MaxRangeLimit { get; set; }
    }
}
