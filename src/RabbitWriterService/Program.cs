using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RabbitWriterService
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureServices((hostContext, services) =>
                {
                    IConfiguration configuration = hostContext.Configuration;

                    string hostName = configuration.GetSection("HostName1").Value;
                    services.AddSingleton(hostName);
                    Console.WriteLine($"hostNamehostNamehostNamehostNamehostNamehostNamehostName{hostName}");

                    List<ControllerOption> options = configuration
                      .GetSection("Controllers")
                      .Get<List<ControllerOption>>();

                    services.AddSingleton(options);
                    services.AddHostedService<RabbitMQWriteWorker>();
                });
    }
}
